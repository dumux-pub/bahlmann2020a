// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Darcy-Subproblem with constant injection of gas at a soil tank bottom (cell-centered finite volume method).
 */
#ifndef DUMUX_DARCY_SUBPROBLEM_GASINJECTION_HH
#define DUMUX_DARCY_SUBPROBLEM_GASINJECTION_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/cctpfa.hh>

#include <dumux/porousmediumflow/1pnc/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include "spatialparams.hh"

#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/material/fluidsystems/h2oairn2.hh>

#include <dumux/material/fluidmatrixinteractions/diffusivitydispersion.hh>

#include <dumux/io/gnuplotinterface.hh>


namespace Dumux
{
template <class TypeTag>
class DarcySubProblem;

namespace Properties
{
// Create new type tags
namespace TTag {
struct DarcyOnePTwoC { using InheritsFrom = std::tuple<OnePNC, CCTpfaModel>; };
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::DarcyOnePTwoC> { using type = Dumux::DarcySubProblem<TypeTag>; };

// The fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DarcyOnePTwoC>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
  using H2OAirN2 = FluidSystems::H2OAirN2<Scalar>;
  static constexpr auto phaseIdx = H2OAirN2::gasPhaseIdx; // simulate the gas phase
  using type = FluidSystems::OnePAdapter<H2OAirN2, phaseIdx>;
};

// Use moles
template<class TypeTag>
struct UseMoles<TypeTag, TTag::DarcyOnePTwoC> { static constexpr bool value = true; };

// Do not replace one equation with a total mass balance
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::DarcyOnePTwoC> { static constexpr int value = 10; };

//! Use an effective difffusivity model with velocity-dependent dispersion
template<class TypeTag>
struct EffectiveDiffusivityModel<TypeTag, TTag::DarcyOnePTwoC>
{ using type = DiffusivityDispersion<GetPropType<TypeTag, Properties::VolumeVariables>>; };

template<class TypeTag>
struct MolecularDiffusionType<TypeTag, TTag::DarcyOnePTwoC>
{ using type = FicksLaw<TypeTag, ReferenceSystemFormulation::molarAveraged>; }; 

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DarcyOnePTwoC> { using type = Dune::YaspGrid<2, Dune::TensorProductCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >; };

// Set the spatial paramaters type
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DarcyOnePTwoC>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = OnePSpatialParams<FVGridGeometry, Scalar>;
};
}

template <class TypeTag>
class DarcySubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    using DiffusionCoefficientAveragingType = typename StokesDarcyCouplingOptions::DiffusionCoefficientAveragingType;
    
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    
    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum {
        // grid and world dimension
        dim = GridView::dimension,
        dimworld = GridView::dimensionworld,

        // primary variable indices
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx,
        compIdx = 2,
    };

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    static constexpr auto dimWorld = FVGridGeometry::GridView::dimensionworld;

public:
    DarcySubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                   std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "Darcy"), eps_(1e-7), couplingManager_(couplingManager)
    {
        // read parameter from input file
        pressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Pressure");
        initialMoleFractionH2O_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialMoleFractionWater");
        initialMoleFractionGas_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialMoleFractionGas");
        
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        std::vector<Scalar> stokesVerticalPositions = getParamFromGroup<std::vector<Scalar>>("Stokes", "Grid.Positions1");
        stokesTop_ = stokesVerticalPositions.back();

        injectionRate_ = getParamFromGroup<Scalar>(this->paramGroup(), "Injection.InjectionRate");
        injectionPointX_ = getParamFromGroup<Scalar>(this->paramGroup(), "Injection.InjectionPointX");
        injectionPointY_ = getParamFromGroup<Scalar>(this->paramGroup(), "Injection.InjectionPointY");
        epsInj_= getParamFromGroup<Scalar>(this->paramGroup(), "Injection.epsInj");
        injectionStop_= getParam<Scalar>("TimeLoop.InjectionStop");
        
        diffCoeffAvgType_ = StokesDarcyCouplingOptions::stringToEnum(DiffusionCoefficientAveragingType{},
                                                                     getParamFromGroup<std::string>(this->paramGroup(), "Problem.InterfaceDiffusionCoefficientAvg"));
        
        advectiveFlux_.resize(this->fvGridGeometry().numDofs());
        diffusiveFlux_.resize(this->fvGridGeometry().numDofs());
        

    }
    //! \brief The destructor
    ~DarcySubProblem()
    {
        fluxFile_.close();
        diffFile_.close();
        advFile_.close();
    }


    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }
    
    void setGridVariables(std::shared_ptr<GridVariables> gridVariables)
    { gridVariables_ = gridVariables; }

     const GridVariables& gridVariables() const
    { return *gridVariables_; }

    template<class SolutionVector, class GridVariables>
    void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables,
                      const Scalar time)
    {
       // compute the fluxes at the interface
        Scalar massComp = 0.0;
        Scalar fluxComp = 0.0;
        Scalar advectiveFluxComp = 0.0;
        Scalar diffusiveFluxComp = 0.0;
        std::vector<double> advFluxInterface;
        std::vector<double> diffFluxInterface;
        std::vector<double> fluxInterface;      
        
        times_.push_back(time);


        Scalar numInterface = 0;
        
        // loop over elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);
            
            GlobalPosition globalPos = element.geometry().center();

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, curSol);

            // loop over sub control volumes and calculate mass
            for (auto&& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                for(int phaseIdx = 0; phaseIdx < FluidSystem::numPhases; ++phaseIdx)
                {
                    massComp += volVars.massFraction(phaseIdx, compIdx)*volVars.density(phaseIdx)
                    * scv.volume() * volVars.saturation(phaseIdx) * volVars.porosity() * volVars.extrusionFactor();
                }
            }
            // loop over sub control volume faces
            for (auto&& scvf : scvfs(fvGeometry))
            {
                // skip loop if face is not part of the interface
                if (!couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
                    continue;
                
                numInterface = numInterface +1;
                xInterface_.push_back(globalPos[0]);

                // NOTE: binding the coupling context is necessary
                couplingManager_->bindCouplingContext(CouplingManager::darcyIdx, element);
                const auto flux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);

                // accumulative fluxes across the interface
                fluxComp += flux[compIdx] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                
                diffusiveFluxComp += (couplingManager().couplingData().diffusiveFluxDarcyBoundary(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_)[2])* scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                
                advectiveFluxComp += (flux[compIdx]- couplingManager().couplingData().diffusiveFluxDarcyBoundary(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_)[2])* scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                
                // fluxes along the interface
                fluxInterface.push_back(flux[compIdx] * FluidSystem::molarMass(compIdx) );
                
                advFluxInterface.push_back((flux[compIdx]- couplingManager().couplingData().diffusiveFluxDarcyBoundary(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_)[2]) * FluidSystem::molarMass(compIdx) );
                
                diffFluxInterface.push_back( (couplingManager().couplingData().diffusiveFluxDarcyBoundary(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_)[2]) * FluidSystem::molarMass(compIdx));

            }
        }
        // convert to kg/s if using mole fractions
        fluxComp =          fluxComp           * FluidSystem::molarMass(compIdx);
        advectiveFluxComp = advectiveFluxComp  * FluidSystem::molarMass(compIdx);
        diffusiveFluxComp = diffusiveFluxComp  * FluidSystem::molarMass(compIdx);
        
        
        // save fluxes along the interface in output files
        if (time ==times_[0])
        {
            fluxFile_.open(problemName_ + "flux_Interface.out");
            fluxFile_ << "Time [s], ";
            std::copy(xInterface_.begin(), xInterface_.end(), std::ostream_iterator<float>(fluxFile_, ", "));
            fluxFile_  << std::endl;
        
            diffFile_.open(problemName_ + "diffFlux.out");
            diffFile_ << "Time [s], ";
            std::copy(xInterface_.begin(), xInterface_.end(), std::ostream_iterator<float>(diffFile_, ", "));
            diffFile_  << std::endl;
                
            advFile_.open(problemName_ + "advFlux.out");
            advFile_ << "Time [s], ";
            std::copy(xInterface_.begin(),
            xInterface_.end(), std::ostream_iterator<float>(advFile_, ", "));
            advFile_  << std::endl;
        }
 
        fluxFile_ << time << ",";
        std::copy(fluxInterface.begin(), fluxInterface.end(), std::ostream_iterator<double>(fluxFile_, ", "));
        fluxFile_  << std::endl;

        diffFile_ << time << ",";
        std::copy(diffFluxInterface.begin(), diffFluxInterface.end(), std::ostream_iterator<double>(diffFile_, ", "));
        diffFile_  << std::endl;

        advFile_ << time << ",";
        std::copy(advFluxInterface.begin(), advFluxInterface.end(), std::ostream_iterator<double>(advFile_, ", "));
        advFile_  << std::endl;
        

        // plot accumulative fluxes in gnuplot
         std::cout<<std::setprecision(12)<<"mass of transportet component is: " << massComp << std::endl;
         std::cout<<"fluxComp from pm "<<fluxComp<<std::endl;

         // total flux
         x_.push_back(time); // in seconds
         y_.push_back(fluxComp);
         gnuplot_.resetPlot();
         gnuplot_.setXRange(0,std::max(time, 86400.0));
         gnuplot_.setYRange(0, 5e-5);
         gnuplot_.setXlabel("time [s]");
         gnuplot_.setYlabel("kg/s");
         gnuplot_.addDataSetToPlot(x_, y_, "fluxComp" + problemName_);
         gnuplot_.plot("fluxHe");

        // advective flux
         x2_.push_back(time); // in seconds
         y2_.push_back(advectiveFluxComp);

         gnuplot2_.resetPlot();
         gnuplot2_.setXRange(0,std::max(time, 86400.0));
         gnuplot2_.setYRange(0, 5e-5);
         gnuplot2_.setXlabel("time [s]");
         gnuplot2_.setYlabel("kg/s");
         gnuplot2_.addDataSetToPlot(x2_, y2_, "advectiveFluxComp" + problemName_);
         gnuplot2_.plot("advectiveFluxComp");

        //diffusive flux
        x3_.push_back(time); // in seconds
        y3_.push_back(diffusiveFluxComp);

        gnuplot3_.resetPlot();
        gnuplot3_.setXRange(0,std::max(time, 86400.0));
        gnuplot3_.setYRange(0, 5e-5);
        gnuplot3_.setXlabel("time [s]");
        gnuplot3_.setYlabel("kg/s");
        gnuplot3_.addDataSetToPlot(x3_, y3_, "diffusiveFluxComp" + problemName_);
        gnuplot3_.plot("diffusiveFluxComp");

    }

    
    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteOutput() const // define output
    { return true; }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 20; } // 20°C
    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
      * \brief Specifies which kind of boundary condition should be
      *        used for which equation on a given boundary control volume.
      *
      * \param element The element
      * \param scvf The boundary sub control volume face
      */
    BoundaryTypes boundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
            values.setAllCouplingNeumann();

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scvf The boundary sub control volume face
     *
     * For this method, the \a values variable stores primary variables.
     */
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        GlobalPosition globalPos = element.geometry().center();

        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
        {
            const auto massFlux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);

            for(int i = 0; i< massFlux.size(); ++i)
                values[i] = massFlux[i];
        }
        
        return values;
    }
    
   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    { 
        NumEqVector values(0.0);
        if(time_ <= injectionStop_ )
        {
            if(globalPos[0] >= injectionPointX_ - epsInj_ && globalPos[0] <= injectionPointX_ + epsInj_ && globalPos[1] <= injectionPointY_ + epsInj_ && globalPos[1] >=injectionPointY_ - epsInj_)
            {
                values[conti0EqIdx + 2] = injectionRate_;
            }
        }
        return values;
    }
    // \}



    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param element The element
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Element &element) const
    {
        PrimaryVariables values(0.0);

        GlobalPosition globalPos = element.geometry().center();
        FluidState fluidState;

        fluidState.setPressure(0, 1e5);
        fluidState.setTemperature(temperature());
        fluidState.setMoleFraction(0, 0, 1);
        Scalar density = FluidSystem::density(fluidState, 0);
        Scalar depth = 0.36 - globalPos[1];

        values[pressureIdx] = pressure_ - density * this->gravity()[dimWorld-1]* (stokesTop_ - globalPos[1]);
        values[conti0EqIdx + 1] = initialMoleFractionH2O_;
        values[conti0EqIdx + 2] = initialMoleFractionGas_;

        return values;
    }
    
    /*!
     * \brief Adds additional VTK output data to the VTKWriter.
     *
     * Function is called by the output module on every write.
     *
     * \param gridVariables The grid variables
     * \param sol The solution vector
     */
    void calculateAdditionalOutput(const SolutionVector& sol, const GridVariables& gridVars)
    {
        std::fill(advectiveFlux_.begin(), advectiveFlux_.end(), 0);
        std::fill(diffusiveFlux_.begin(), diffusiveFlux_.end(), 0);

        //this is for the additional injected component
        auto upwindTerm = [](const auto& volVars) { return volVars.mobility(0)*volVars.density(0)*volVars.moleFraction(0, 2); };
        
        // loop over elements and evaluate the total flux, the diffusive flux and the advective flux
        for (const auto& element : elements(this->fvGridGeometry().gridView(), Dune::Partitions::interior))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVars.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, sol);

            auto elemFluxVars = localView(gridVars.gridFluxVarsCache());
            elemFluxVars.bind(element, fvGeometry, elemVolVars);

            using FluxVariables =  GetPropType<TypeTag, Properties::FluxVariables>;
            
            // loop over sub control volumes
            for (auto&& scv : scvs(fvGeometry))
            {
                auto ccDofIdx = scv.dofIndex();
                
                // loop over sub control volume faces
                for (auto&& scvf: scvfs(fvGeometry))
                {
                    if (!scvf.boundary())
                    {
                        auto dirIdx = 0.0;

                        if (scvf.unitOuterNormal()[0] == 0)
                             dirIdx = 1.0;
                        FluxVariables fluxVars;
                        fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVars);
                        advectiveFlux_[ccDofIdx][dirIdx] += 0.5*fluxVars.advectiveFlux(0, upwindTerm);

                        diffusiveFlux_[ccDofIdx][dirIdx] += 0.5*fluxVars.molecularDiffusionFlux(0)[2];
                    }
                    else if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
                    {
                        // NOTE: binding the coupling context is necessary
                        couplingManager_->bindCouplingContext(CouplingManager::darcyIdx, element);
                        
                        auto dirIdx = 0.0;

                        if (scvf.unitOuterNormal()[0] == 0)
                             dirIdx = 1.0;
                     
                        advectiveFlux_[ccDofIdx][dirIdx] += 0.5*( couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_)[2]- couplingManager().couplingData().diffusiveFluxDarcyBoundary(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_)[2]);
                        
                        diffusiveFlux_[ccDofIdx][dirIdx] +=0.5*couplingManager().couplingData().diffusiveFluxDarcyBoundary(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_)[2];
                    }
                }
            }
        }
    }
    
    
    const std::vector<Dune::FieldVector<Scalar, 2>>&getAdvectiveFlux() const
    { return advectiveFlux_; }

    const std::vector<Dune::FieldVector<Scalar, 2>>&getDiffusiveFlux() const
    { return diffusiveFlux_; }
    

    
    /*!
    * \brief Set the simulation time.
    *
    * \param t The current time.
    */
    void setTime(Scalar t)
    { time_ = t; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->fvGridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_; }
    
    
    
    Scalar time_;
    Scalar injectionStop_;
    Scalar eps_;
    Scalar pressure_;
    Scalar initialMoleFractionH2O_;
    Scalar stokesTop_;
    Scalar initialMoleFractionGas_;
    Scalar injectionRate_;
    Scalar injectionPointX_;
    Scalar injectionPointY_;
    Scalar epsInj_;
    std::string problemName_;
    std::shared_ptr<CouplingManager> couplingManager_;
    DiffusionCoefficientAveragingType diffCoeffAvgType_;
    std::shared_ptr<GridVariables> gridVariables_;

    std::vector<Dune::FieldVector<Scalar, 2>> advectiveFlux_;
    std::vector<Dune::FieldVector<Scalar, 2>> diffusiveFlux_;

    std::vector<double> times_;
    std::vector<double> x_;
    std::vector<double> y_;
    Dumux::GnuplotInterface<double> gnuplot_;
    std::vector<double> y2_;
    std::vector<double> x2_;
    Dumux::GnuplotInterface<double> gnuplot2_;
    std::vector<double> y3_;
    std::vector<double> x3_;
    Dumux::GnuplotInterface<double> gnuplot3_;
    
    std::vector<double> xInterface_;
    std::vector<std::vector<double>> fluxInterface_;
    std::vector<std::vector<double>> advFluxInterface_;
    std::vector<std::vector<double>> diffFluxInterface_;
    
    std::ofstream fluxFile_;
    std::ofstream diffFile_;
    std::ofstream advFile_;
};
} //end namespace

#endif //DUMUX_DARCY_SUBPROBLEM_HH
