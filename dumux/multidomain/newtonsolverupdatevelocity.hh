// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Nonlinear
 * \ingroup MultiDomain
 * \copydoc Dumux::MultiDomainNonEquilPriVarSwitchNewtonSolver
 */
#ifndef DUMUX_MULTIDOMAIN_NONEQUIL_PRIVARSWITCH_NEWTON_SOLVER_HH
#define DUMUX_MULTIDOMAIN_NONEQUIL_PRIVARSWITCH_NEWTON_SOLVER_HH

#include <memory>

#include <dumux/common/typetraits/utility.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/multidomain/newtonsolver.hh>

namespace Dumux {

/*!
 * \ingroup Nonlinear
 * \ingroup MultiDomain
 * \brief A newton solver that handles primary variable switches for multi domain problems
 */
template <class Assembler, class LinearSolver, class CouplingManager,
          class Reassembler = DefaultPartialReassembler,
          class Comm = Dune::CollectiveCommunication<Dune::MPIHelper::MPICommunicator> >
class MultiDomainGridVariablesUpdateNewtonSolver : public MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager, Reassembler, Comm >
{
    using Scalar =  typename Assembler::Scalar;
    using ParentType = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager, Reassembler, Comm>;
    using SolutionVector = typename Assembler::ResidualType;

    template<std::size_t i>
    using PrimaryVariableSwitch = typename Detail::GetPVSwitchMultiDomain<Assembler, i>::type;

    template<std::size_t i>
    using HasPriVarsSwitch = typename Detail::GetPVSwitchMultiDomain<Assembler, i>::value_t; // std::true_type or std::false_type

    template<std::size_t i>
    using PrivarSwitchPtr = std::unique_ptr<PrimaryVariableSwitch<i>>;
    using PriVarSwitchPtrTuple = typename Assembler::Traits::template Tuple<PrivarSwitchPtr>;

public:
    using ParentType::ParentType;

    /*!
     * \brief Indicates that one Newton iteration was finished.
     *
     * \param assembler The jacobian assembler
     * \param uCurrentIter The solution after the current Newton iteration
     * \param uLastIter The solution at the beginning of the current Newton iteration
     */
    void newtonEndStep(SolutionVector &uCurrentIter,
                       const SolutionVector &uLastIter)
    {
        ParentType::newtonEndStep(uCurrentIter, uLastIter);

        // update the variable switch (returns true if the pri vars at at least one dof were switched)
        // for disabled grid variable caching
        auto& assembler = this->assembler();
        constexpr auto darcyIdx = CouplingManager::darcyIdx;
        auto& gridVariablesdarcy = assembler.gridVariables(darcyIdx);
        // Averages the face velocities of a vertex. Implemented in the model.
        // The velocities are stored in the model.
        gridVariablesdarcy.calcVelocityAverage(uCurrentIter[darcyIdx]);
    }


private:
    //! the class handling the primary variable switch
    PriVarSwitchPtrTuple priVarSwitches_;
    //! if we switched primary variables in the last iteration
    std::array<bool, Assembler::Traits::numSubDomains> switchedInLastIteration_;
};

} // end namespace Dumux

#endif
