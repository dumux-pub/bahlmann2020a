// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Binary coefficients for water and methane.
 */
#ifndef DUMUX_BINARY_COEFF_H2O_He_HH
#define DUMUX_BINARY_COEFF_H2O_He_HH

#include <dumux/material/binarycoefficients/henryiapws.hh>
#include <dumux/material/binarycoefficients/fullermethod.hh>

#include <dumux/material/components/helium.hh>
#include <dumux/material/components/h2o.hh>

namespace Dumux
{
namespace BinaryCoeff
{

/*!
 * \ingroup Binarycoefficients
 * \brief Binary coefficients for water and helium.
 */
class H2O_He
{
public:
  /*!
     * \brief Henry coefficient \f$\mathrm{[Pa]}\f$  for molecular helium in liquid water.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * http://www.iapws.org/relguide/HenGuide.pdf
     */
    template <class Scalar>
    static Scalar henry(Scalar temperature)
    {
        const Scalar E = 2267.4082;
        const Scalar F = -2.9616;
        const Scalar G = -3.2604;
        const Scalar H = 7.8819;

        return henryIAPWS(E, F, G, H, temperature);
    }

    /*!
     * \brief Binary diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular water in helium.
     *
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$
     */
    template <class Scalar>
    static Scalar gasDiffCoeff(Scalar temperature, Scalar pressure)
    {
        using H2O = Components::H2O<Scalar>;
        using He =  Components::He<Scalar>;
        
        //         DUNE_THROW(Dune::NotImplemented, "diffusion coefficient for gasous water and helium");

//         typedef Dumux::H2O<Scalar> H2O;
//         typedef Dumux::He<Scalar> He;

        // atomic diffusion volumes
        // Vch4 = sum(ni*Vi) = 15.9 + 4*2.31 = 25.14 (Tang et al., 2015)--> method, (Reid et al., 1987)--> values
        const Scalar SigmaNu[2] = { 13.1 /* H2O */,  2.67 /* He */ };
        // molar masses [g/mol]
        const Scalar M[2] = { H2O::molarMass()*Scalar(1e3), He::molarMass()*Scalar(1e3) };

        return fullerMethod(M, SigmaNu, temperature, pressure);
    }

    /*!
     * \brief Diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular methane in liquid water.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$
     *
     * The empirical equations for estimating the diffusion coefficient in
     * infinite solution which are presented in Reid, 1987 \cite reid1987 all show a
     * linear dependency on temperature. We thus simply scale the
     * experimentally obtained diffusion coefficient of Ferrell and
     * Himmelblau by the temperature.<br>
     * This function use an interpolation of the data by \cite Whitherspoon1965
     * TODO:liquid diffusion for helium in water
     *
     * \url http://onlinelibrary.wiley.com/doi/10.1002/aic.690130421/epdf
     */
    template <class Scalar>
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    {
//         DUNE_THROW(Dune::NotImplemented, "diffusion coefficient for liquid water and helium");
        return 6e-9; /*(Ferrel and Himmelblau, 1967)*//*return ??? * temperature - ???;
    */}
};

}
} // end namespace

#endif
