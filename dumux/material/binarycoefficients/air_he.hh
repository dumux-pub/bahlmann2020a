// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Binary coefficients for air and methane.
 */
#ifndef DUMUX_BINARY_COEFF_Air_He_HH
#define DUMUX_BINARY_COEFF_Air_He_HH

#include <dumux/material/binarycoefficients/henryiapws.hh>
#include <dumux/material/binarycoefficients/fullermethod.hh>

#include <dumux/material/components/air.hh>
#include <dumux/material/components/helium.hh>

namespace Dumux
{
namespace BinaryCoeff
{

/*!
 * \ingroup Binarycoefficients
 * \brief Binary coefficients for air and methane.
 */
class Air_He
{
public:
    /*!
     * \brief Henry coefficient \f$\mathrm{[Pa]}\f$  for molecular methane in air.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     */
    template <class Scalar>
    static Scalar henry(Scalar temperature)
    {
        DUNE_THROW(Dune::NotImplemented, "Henry coefficient for methane in liquid air");
    }

    /*!
     * \brief Binary diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular methane in gaseous air.
     *
     * Uses fullerMethod to determine the diffusion of methane in air.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$
     * TODO:atomic diffusion volume
     */
    template <class Scalar>     
    static Scalar gasDiffCoeff(Scalar temperature, Scalar pressure)
    {
        using Air = Components::Air<Scalar>;
        using He = Components::He<Scalar>;
        
//         typedef Dumux::Air<Scalar> Air;
//         typedef Dumux::He<Scalar> He;

        // atomic diffusion volumes
            // Vair = 19.7 (Reid et al., 1987) or 20.1 (Fuller et al.,1966)
            // VHe = sum(ni*Vi) = 2.67 (Reid et al., 1987)--> values
        const Scalar SigmaNu[2] = { 19.7 /* Air */,  2.67 /* He */ };
        // molar masses [g/mol]
        const Scalar M[2] = { Air::molarMass()*1e3, He::molarMass()*1e3 };
        return fullerMethod(M, SigmaNu, temperature, pressure);
    }

    /*!
     * \brief Diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for liquid air or methane.
     * \param temperature temperature in \f$\mathrm{[K]}\f$
     * \param pressure pressure in \f$\mathrm{[Pa]}\f$
     */
    template <class Scalar>
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    {
        DUNE_THROW(Dune::NotImplemented, "Diffusion coefficient for liquid air or methane");
    }
};

}
} // end namespace

#endif
