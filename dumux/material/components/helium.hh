// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup Components
 *
 * \brief Properties of helium \f$He\f$.
 * https://webbook.nist.gov/cgi/inchi?ID=C7440597&Mask=4
 */
#ifndef DUMUX_HE_HH
#define DUMUX_HE_HH

#include <dumux/material/idealgas.hh>

#include <dumux/material/components/base.hh>
#include <dumux/material/components/gas.hh>

#include <cmath>

namespace Dumux{
namespace Components{

/*!
 * \ingroup Components
 *
 * \brief Properties of pure molecular helium \f$He\f$.
 *
 * \tparam Scalar The type used for scalar values
 */
template <class Scalar>
class He 
: public Components::Base<Scalar, He<Scalar> >
, public Components::Gas<Scalar, He<Scalar> >
{
    using IdealGas = Dumux::IdealGas<Scalar>;

public:
    /*!
     * \brief A human readable name for helium.
     */
    static const char *name()
    { return "He"; }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of molecular helium.
     */
    static Scalar molarMass()
    { return 4.0026e-3; /* [kg/mol] */}

    /*!
     * \brief Returns the critical temperature \f$\mathrm{[K]}\f$ of molecular helium
     */
    static Scalar criticalTemperature()
    { return 5.19; /* [K] */ }

    /*!
     * \brief Returns the critical pressure \f$\mathrm{[Pa]}\f$ of molecular helium
     */
    static Scalar criticalPressure()
    { return 2.27e5; /* [Pa] */ }

    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ at molecular helium's triple point.
     */
    static Scalar tripleTemperature()
    { return 2.1768; /* [K] */ }// wikipedia: https://en.wikipedia.org/wiki/Triple_point

    /*!
     * \brief Returns the pressure \f$\mathrm{[Pa]}\f$ at molecular helium's triple point.
     */
    static Scalar triplePressure()
    { return 5048.0; /* [Pa] */ } // wikipedia:https://en.wikipedia.org/wiki/Triple_point

    /*!
     * \brief The vapor pressure in \f$\mathrm{[Pa]}\f$ of pure molecular helium
     *        at a given temperature. 
     *
     *\param T temperature of component in \f$\mathrm{[K]}\f$
     */
    static Scalar vaporPressure(Scalar T)
    { 
         DUNE_THROW(Dune::NotImplemented, "vaporPressure for Helium"); 
        // copied from nitrogen:
//         if (T > criticalTemperature())
//             return criticalPressure();
//         if (T < tripleTemperature())
//             return 0; // He is solid: We don't take sublimation into
//                       // account
                      
    }

    /*!
     * \brief Returns true if the gas phase is assumed to be compressible
     */
    static bool gasIsCompressible()
    { return true; }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of \f$CH_4\f$ gas at a given pressure and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar gasDensity(Scalar temperature, Scalar pressure)
    {
        // Assume an ideal gas
        return IdealGas::density(molarMass(), temperature, pressure);
    }
    
        /*!
     *  \brief The molar density of CO2 gas in \f$\mathrm{[mol/m^3]}\f$ at a given pressure and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     *
     */
    static Scalar gasMolarDensity(Scalar temperature, Scalar pressure)
    { return gasDensity(temperature, pressure)/molarMass(); }


    /*!
     * \brief Returns true iff the gas phase is assumed to be ideal
     */
    static bool gasIsIdeal()
    { return true; }

    /*!
     * \brief The pressure of gaseous \f$CH_4\f$ in \f$\mathrm{[Pa]}\f$ at a given density and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param density density of component in \f$\mathrm{[kg/m^3]}\f$
     */
    static Scalar gasPressure(Scalar temperature, Scalar density)
    {
        // Assume an ideal gas
        return IdealGas::pressure(temperature, density/molarMass());
    }

    /*!
     * \brief Specific enthalpy \f$\mathrm{[J/kg]}\f$ of pure helium gas.
     *
     * \param T temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     *
     * See: R. Reid, et al. (1987, pp 154, 657, 671) \cite reid1987
     * TODO: find values for helium
     */
    static const Scalar gasEnthalpy(Scalar temperature,
                                    Scalar pressure)
    {
        return gasHeatCapacity(temperature, pressure) * temperature;
    }


    /*!
     * \brief Specific enthalpy \f$\mathrm{[J/kg]}\f$ of pure helium gas.
     *
     *        Definition of enthalpy: \f$h= u + pv = u + p / \rho\f$.
     *
     *        Rearranging for internal energy yields: \f$u = h - pv\f$.
     *
     *        Exploiting the Ideal Gas assumption (\f$pv = R_{\textnormal{specific}} T\f$)gives: \f$u = h - R / M T \f$.
     *
     *        The universal gas constant can only be used in the case of molar formulations.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */

    static const Scalar gasInternalEnergy(Scalar temperature,
                                          Scalar pressure)
    {

        return
            gasEnthalpy(temperature, pressure) -
            1/molarMass()* // conversion from [J/(mol K)] to [J/(kg K)]
            IdealGas::R*temperature; // = pressure * spec. volume for an ideal gas
    }


    /*!
     * \brief Specific isobaric heat capacity \f$\mathrm{[J/(kg*K)]}\f$ of pure
     *        air.
     *
     *  This method uses the Shomate equation to calculate the heat capacity of He. It is valid for temperature betwenn 298. - 6000 K
     *  https://webbook.nist.gov/cgi/inchi?ID=C7440597&Mask=1#Thermo-Gas
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * 
     * TODO:find heat capacity for temperatures smaller 298 K
     *
     */
    static const Scalar gasHeatCapacity(Scalar temperature,
                                        Scalar pressure)
    {
        Scalar t= temperature;

        const Scalar A = 20.78603;
        const Scalar B = 4.850638e-10;
        const Scalar C = -1.582916e-10;
        const Scalar D = 1.525102e-11;
        const Scalar E = 3.196347e-11;
        
        return  
            1/molarMass()*                          // conversion from [J/(mol*K)] to [J/(kg*K)]
            (A + B*t + C*t*t + D*t*t*t + E/(t*t) );    // [J/(kg*K]
    }

    /*!
     * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of \f$He\f$ at a given pressure and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     *
     * See:
     * http://orbit.dtu.dk/files/52768509/ris_224.pdf
     *
     */
    static Scalar gasViscosity(Scalar temperature, Scalar pressure)

    {
        using std::pow;
        Scalar mu = 3.674e-7*pow(temperature,0.7);

        return mu;
    }

   /*!
     * \brief The thermal Conductivity  at a given pressure and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     * \todo find suitable definition
     * http://orbit.dtu.dk/files/52768509/ris_224.pdf
     * TODO: find method dependent on temperature and pressure
     */
    static Scalar gasThermalConductivity(Scalar temperature, Scalar pressure)
    {
        using std::pow;
        return 2.682e-3 * (1 + 1.123e-3 * pressure) * pow(temperature, 0.71 * (1 - 2e-4 * pressure));
    }

};
} // end namespace Components
} // end namespace Dumux

#endif
