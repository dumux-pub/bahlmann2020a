// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidmatrixinteractions
 * \brief   Relation for the saturation-dependent effective diffusion coefficient
 */
#ifndef DIFFUSIVITY_DISPERSION_HH
#define DIFFUSIVITY_DISPERSION_HH

#include <cmath>
#include <dumux/porousmediumflow/velocityoutput.hh>


namespace Dumux {

/*!
 * \ingroup Fluidmatrixinteractions
 * \brief Relation for the saturation-dependent effective diffusion coefficient including velocity-dependent dispersion
 *
 * The material law for isotropic material is:
 * \f[
 *  D_\text{eff,pm} = \phi * S_w * \tau * D + D_disp
 * \f]
 *
 * with
 * \f[
 *  D_disp = alpha (v_x² + v_y² + v_z²)/sqrt(v_x²+v_y²+v_z²)
 * \f]
 *
 * after Bear 1972: <i>On the tensor form if Dispersion in Porous Media</i> \cite Bear1972
 * and Kalejaiye 2005:: <i>Specification of the dispersion coefficient in the modeling of gravity-driven flow in porous media</i> \cite Kalejaiye2005
 */
template<class VolumeVariables>
class DiffusivityDispersion
{
    using Scalar = typename VolumeVariables::PrimaryVariables::value_type;

public:
    /*!
     * \brief Returns the effective diffusion coefficient \f$\mathrm{[m^2/s]}\f$ on a constant tortuosity value and velocity-dependent dispersion.
     *
     * \param porosity The porosity
     * \param saturation The saturation of the phase
     * \param diffCoeff The diffusion coefficient of the phase \f$\mathrm{[m^2/s]}\f$
     * \param vx velocity in x direction
     * \param vy velocity in y direction
     * \param vz velocity in z direction
     */
    static Scalar effectiveDiffusivity(const Scalar porosity,
                                       const Scalar saturation,
                                       const Scalar diffCoeff,
                                       const Scalar velocityMagnitude)
    {
        static const Scalar tau = getParam<Scalar>("SpatialParams.Tortuosity", 1.0);
        static const Scalar alpha = getParam<Scalar>("SpatialParams.DispersionAlpha");
//         std::cout<<"Dispersion"<<alpha * velocityMagnitude<<std::endl;
//         std::cout<<"Geschwindigkeit: "<<velocityMagnitude<<std::endl;
//         const int phaseIdx = 0; //gas phase only
//         VolumeVariables volVars;
//         using std::sqrt;
//         std::cout<< "vx " << volVars.velocity(phaseIdx)[0] << " vz " << volVars.velocity(phaseIdx)[1] << std::endl;
//         Scalar vx = volVars.velocity(phaseIdx)[0];
//         Scalar vy = volVars.velocity(phaseIdx)[1];
//         Scalar vz = volVars.velocity(phaseIdx)[2];
//         std::cout<< "Dispersion:  " <<  alpha *(vx*vx + vy*vy + vz*vz) / sqrt(vx*vx + vy*vy  + vz*vz) << std::endl;
//         Scalar darcyMagVelocity = volVars.darcyMagVelocity(phaseIdx);
        

        return porosity * saturation * diffCoeff * tau + alpha * velocityMagnitude;
    }
};
}
#endif
